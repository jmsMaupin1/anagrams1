let hashMap = {};

function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}

function loadAnagramHashMap() {
    let hmap = words.reduce( (pre, cur, index, arr) => {
        let key = alphabetize(cur);
        if(!pre[key]) pre[key] = [cur];
        else pre[key].push(cur)
        return pre;
    }, {})

    return hmap
}

function findAnagrams(string) {
    let key = alphabetize(string);
    hashmap = loadAnagramHashMap();

    return hashmap[key]
}

const button = document.getElementById("findButton");
button.onclick = function () {
    let typedText = document.getElementById("input").value;
    let anagrams = findAnagrams(typedText);

    document.getElementById("output").textContent = anagrams
}